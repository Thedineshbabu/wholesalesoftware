import { WholeSaleWebPage } from './app.po';

describe('whole-sale-web App', function() {
  let page: WholeSaleWebPage;

  beforeEach(() => {
    page = new WholeSaleWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
